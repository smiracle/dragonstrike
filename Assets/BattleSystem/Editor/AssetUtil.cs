﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class AssetUtil {
	
	//Make assumption that the base class we pass in is always a Scriptable object
	public static void CreateScriptableObject<T>() where T : ScriptableObject
	{		
		var asset = ScriptableObject.CreateInstance<T> ();

		var path = AssetDatabase.GetAssetPath (Selection.activeObject);

		//Appends a number to the end of the path
		var assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (path + "/New" + typeof(T) + ".asset");

		AssetDatabase.CreateAsset (asset, assetPathAndName);
		Selection.activeObject = asset;
		EditorUtility.FocusProjectWindow ();
		AssetDatabase.SaveAssets ();
	}
}
